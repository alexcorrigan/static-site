#Static Site

Simple little thing to run a static website inside a docker container.

To build:

`docker-compose build`

To run:

`docker-compose up -d`

To stop:

`docker-compose down`

The site will be accessable at `http://localhost:8080`
